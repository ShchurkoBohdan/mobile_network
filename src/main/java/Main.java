import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import view.View;

public class Main {
    public static final Logger loger = LogManager.getLogger(Main.class);
    public static void main(String[] args){
        new View().start();
    }
}
