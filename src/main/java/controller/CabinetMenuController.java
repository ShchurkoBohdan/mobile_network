package controller;

import model.Call;
import model.SMS;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.NetworkService;
import service.PhoneService;
import service.UserCabinetService;
import utils.StopWatch;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CabinetMenuController implements Controller {
    public static final Logger logger = LogManager.getLogger(WelcomeMenuController.class);
    private User user;

    public CabinetMenuController() {
    }

    public CabinetMenuController(User user) {
        this.user = user;
    }

    public void getAccountInfo() {
        user.print();
        try {
            new NetworkService().getNetworkProvider(user).print();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void showBalance() {
        logger.info(String.format("User balance - " + new UserCabinetService().getBalance(user) + " UAH\n"));
    }

    public void refillBalance() {
        Scanner scanner = new Scanner(System.in);
        logger.info("Enter amount you want to add to user balance (0.00): ");
        double amountToAdd = scanner.nextDouble();
        int resultStatus = new UserCabinetService().refillBalance(user, amountToAdd);
        if (resultStatus == 1) {
            logger.trace(String.format("You have successfuly updated your balance for %s UAH", Double.toString(amountToAdd)));
        }
    }

    public void makeCall() {
        String endCall = "";
        int callTimeInMin = 0;
        String numberToCall = "";
        double callPrice = 0;
        if (new UserCabinetService().getBalance(user) > 0) {
            Scanner scanner = new Scanner(System.in);
            logger.info("Enter phone number to call (in format: 0XXXXXXXXX): ");
            numberToCall = scanner.nextLine();
            int phoneNumberNetworkProviderID = new NetworkService().detectNetworkProvider(numberToCall);
            try {
                if (phoneNumberNetworkProviderID == new NetworkService().getNetworkProvider(user).getId()) {
                    callPrice = new NetworkService().getNetworkProvider(user).getCall_in_network_price();
                } else {
                    callPrice = new NetworkService().getNetworkProvider(user).getCall_out_network_price();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (new UserCabinetService().getBalance(user) > callPrice) {
                one:
                do {
                    StopWatch stopWatch = new StopWatch();
                    double maxTimeOfCall = Math.round(new UserCabinetService().getBalance(user) / callPrice);
                    System.out.println(new UserCabinetService().getBalance(user) + " / " + callPrice + " = " + maxTimeOfCall);
                    while (!endCall.equals("end")) {
                        logger.info("Enter 'end' to finish your call\n");
                        endCall = scanner.nextLine();
                        if (!endCall.equals("end")) {
                            logger.error("Incorrect data");
                        }
                    }
                    callTimeInMin = (int) (stopWatch.getTime() / 60);
                    if (stopWatch.getTime() % 60 != 0) {
                        callTimeInMin = callTimeInMin + 1;
                    }
                } while (!endCall.equals("end"));
                logger.debug("End call");
                logger.debug("Your call lasted " + callTimeInMin + " min.\n");
                double amountToDeduct = callTimeInMin * callPrice;
                int rowsUpdated = new UserCabinetService().deductBalance(user, amountToDeduct);
                int rowsUpdated1 = new PhoneService().registerCall(numberToCall, callTimeInMin, user.getId());
            } else {
                logger.error("Insufficient funds. Please update your balance\n");
            }
        } else {
            logger.error("Insufficient funds. Please update your balance\n");
        }
    }

    public void getCallHistory() {
        List<Call> calls = new ArrayList<>();
        try {
            calls = new PhoneService().getCallHistory(user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        logger.debug("LIST OF CALLS: ");
        calls.forEach(c -> c.print());
    }

    public void sendSMS() {
        Scanner scanner = new Scanner(System.in);
        logger.info("Enter phone number to send SMS to (in format: 0XXXXXXXXX): ");
        String receiverNumber = scanner.nextLine();
        double smsPrice = 0;
        int smsCount = 0;
        int phoneNumberNetworkProviderID = new NetworkService().detectNetworkProvider(receiverNumber);
        try {
            if (phoneNumberNetworkProviderID == new NetworkService().getNetworkProvider(user).getId()) {
                smsPrice = new NetworkService().getNetworkProvider(user).getSms_in_network_price();
            } else {
                smsPrice = new NetworkService().getNetworkProvider(user).getSms_out_network_price();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        logger.info("Enter SMS body text: ");
        String smsText = scanner.nextLine();
        try {
            if (smsText.length() % new NetworkService().getNetworkProvider(user).getChars_in_sms() == 0){
                smsCount = smsText.length() / new NetworkService().getNetworkProvider(user).getChars_in_sms();
            }else {
                smsCount = smsText.length() / new NetworkService().getNetworkProvider(user).getChars_in_sms() + 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        double amountToDeduct = smsCount*smsPrice;
        if(amountToDeduct <= new UserCabinetService().getBalance(user)){
            int rowsUpdaetd = new UserCabinetService().deductBalance(user, amountToDeduct);
            int rowsUpdated1 = new PhoneService().registerSMS(receiverNumber, smsText, user.getId());
            logger.debug("Your message is sent.");
        }else {
            logger.error("Insufficient funds. Please update your balance\n");
        }
    }

    public void getSmsHistory() {
        List<SMS> sms = new ArrayList<>();
        try {
            sms = new PhoneService().getSmsHistory(user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        logger.debug("LIST OF SMS: ");
        sms.forEach(s -> s.print());
    }

    public void logOut() {
        logger.trace("Back to Login menu\n");
    }

    @Override
    public void print() {
    }
}
