package controller;

@FunctionalInterface
public interface Controller {
    void print();
}
