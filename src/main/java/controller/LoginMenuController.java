package controller;

import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.LoginService;
import view.menu.CabinetMenu;

import java.sql.SQLException;
import java.util.Objects;
import java.util.Scanner;

public class LoginMenuController implements Controller {
    public static final Logger logger = LogManager.getLogger(LoginMenuController.class);

    public void provideUserCredentials(){
        Scanner scanner = new Scanner(System.in);
        logger.info("Enter username:");
        String username = scanner.nextLine();
        logger.info("Enter password:");
        String password = scanner.nextLine();
        try {
            User user = new LoginService().getUser(username, password);
            if ((!Objects.isNull(user)) && user.getUsername().equals(username) && user.getPassword().equals(password)){
                logger.debug("Hello, " + user.getName() + " " + user.getSurname());
                new CabinetMenu(user).outputSubCabinetMenu();
            }else {
                logger.error("User not found\n");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void back(){
        logger.trace("Back to Main menu.\n");
    }

    @Override
    public void print() {}
}
