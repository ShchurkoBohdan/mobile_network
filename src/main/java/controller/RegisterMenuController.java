package controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.NetworkService;
import service.RegisterService;

import java.util.Scanner;

public class RegisterMenuController implements Controller {
    public static final Logger logger = LogManager.getLogger(RegisterMenuController.class);
    Scanner scanner = new Scanner(System.in);

    public void createUser() {
        logger.info("Provide Name:");
        String name = scanner.nextLine();
        logger.info("Provide Surname:");
        String surname = scanner.nextLine();
        logger.info("Provide Username:");
        String username = scanner.nextLine();
        logger.info("Provide Password:");
        String password = scanner.nextLine();
        int countOfUsers = new RegisterService().createUser(name, surname, username, password);
        int countOfPhones = createPhone(name, surname, username, password);
        logger.trace(String.format("%d user was created with %d mobile.\n", countOfUsers, countOfPhones));
        logger.trace("Back to Register menu.\n");
    }

    public int createPhone(String name, String surname, String username, String password) {
        logger.debug("Сreate your Mobile:\n");
        logger.info("Provide Model:");
        String model = scanner.nextLine();
        logger.info("Provide Number (in format: 0XXXXXXXXX):");
        String number = scanner.nextLine();
        int networkProviderId = detectNetworkPrivider(number);
        int countOfPhones = new RegisterService().createPhone(name, surname, username, password, model, number, networkProviderId);
        return countOfPhones;
    }

    public int detectNetworkPrivider(String number){
        int providerId = new NetworkService().detectNetworkProvider(number);
        return providerId;
    }

    public void back() {
        logger.trace("Back to Main menu.\n");
    }

    @Override
    public void print() {}
}
