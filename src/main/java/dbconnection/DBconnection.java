package dbconnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

public class DBconnection {
    public static Logger logger = LogManager.getLogger(DBconnection.class);
    public static final String URL = "jdbc:mysql://localhost:3306/networking_DB";
    public static final String USER = "root";
    public static final String PASSWORD = "Shchurko1";

    public static Connection connection = null;

    public static Connection getDBConnection() {
        if (Objects.isNull(connection)) {
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                connection = DriverManager.getConnection(URL, USER, PASSWORD);
                if (connection.isValid(100)) {
                    logger.trace("DB connection is set up successfully.\n");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }
}
