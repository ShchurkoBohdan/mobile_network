package model;

import model.annotations.Column;
import model.annotations.Table;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.PhoneService;

@Table(name = "calls")
public class Call {
    public static final Logger logger = LogManager.getLogger(Call.class);
    @Column(name = "id")
    private int id;
    @Column(name = "receiver")
    private String receiver;
    @Column(name = "call_time")
    private int call_time;
    @Column(name = "user_id")
    private int user_id;

    public Call() {
    }

    public Call(String receiver, int call_time, int user_id) {
        this.receiver = receiver;
        this.call_time = call_time;
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public int getCall_time() {
        return call_time;
    }

    public void setCall_time(int call_time) {
        this.call_time = call_time;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void print(){
        logger.info("\n\t---------------------------------" +
                "\n\tReceiver: " + getReceiver() +
                "\n\tTime of call: " + getCall_time() + "min" +
                "\n\t---------------------------------");
    }
}
