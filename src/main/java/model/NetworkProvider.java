package model;

import model.annotations.Column;
import model.annotations.Table;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Table(name = "network_provider")
public class NetworkProvider {
    public static final Logger logger = LogManager.getLogger(NetworkProvider.class);
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "call_in_network_price")
    private double call_in_network_price;
    @Column(name = "call_out_network_price")
    private double call_out_network_price;
    @Column(name = "sms_in_network_price")
    private double sms_in_network_price;
    @Column(name = "sms_out_network_price")
    private double sms_out_network_price;
    @Column(name = "chars_in_sms")
    private int chars_in_sms;

    public NetworkProvider() {
    }

    public NetworkProvider(String name, double call_in_network_price, double call_out_network_price, double sms_in_network_price, double sms_out_network_price, int chars_in_sms) {
        this.name = name;
        this.call_in_network_price = call_in_network_price;
        this.call_out_network_price = call_out_network_price;
        this.sms_in_network_price = sms_in_network_price;
        this.sms_out_network_price = sms_out_network_price;
        this.chars_in_sms = chars_in_sms;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCall_in_network_price() {
        return call_in_network_price;
    }

    public void setCall_in_network_price(double call_in_network_price) {
        this.call_in_network_price = call_in_network_price;
    }

    public double getCall_out_network_price() {
        return call_out_network_price;
    }

    public void setCall_out_network_price(double call_out_network_price) {
        this.call_out_network_price = call_out_network_price;
    }

    public double getSms_in_network_price() {
        return sms_in_network_price;
    }

    public void setSms_in_network_price(double sms_in_network_price) {
        this.sms_in_network_price = sms_in_network_price;
    }

    public double getSms_out_network_price() {
        return sms_out_network_price;
    }

    public void setSms_out_network_price(double sms_out_network_price) {
        this.sms_out_network_price = sms_out_network_price;
    }

    public int getChars_in_sms() {
        return chars_in_sms;
    }

    public void setChars_in_sms(int chars_in_sms) {
        this.chars_in_sms = chars_in_sms;
    }

    public void print() {
        logger.info("\tNETWORK INFO" +
                "\n------------------------------" +
                "\nName: " + getName() +
                "\nCall in network price/min: " + getCall_in_network_price() +
                "\nCall out of network price/min: " + getCall_out_network_price() +
                "\nSMS in network price/sms: " + getSms_in_network_price() +
                "\nSMS out of network price/sms: " + getSms_out_network_price() +
                "\n------------------------------");
    }
}
