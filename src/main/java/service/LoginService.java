package service;

import dbconnection.DBconnection;
import dbconnection.dbdata.DataConverter;
import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginService {
    public static final String GET_USER = "select * from user where username=? and password=?";

    public User getUser(String username, String password) throws SQLException {
        Connection connection = DBconnection.getDBConnection();
        ResultSet rs = null;
        User user = null;
        try {
            PreparedStatement ps = connection.prepareStatement(GET_USER);
            ps.setString(1, username);
            ps.setString(2, password);
            rs = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (rs.next()){
            user = (User) new DataConverter(User.class).fromResultSetToEntity(rs);
        }
        return user;
    }
}
