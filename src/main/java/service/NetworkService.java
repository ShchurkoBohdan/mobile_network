package service;

import dbconnection.DBconnection;
import dbconnection.dbdata.DataConverter;
import model.NetworkProvider;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NetworkService {
    public static final Logger logger = LogManager.getLogger(NetworkService.class);
    public static final String GET_PROVIDER = "select id, name, call_in_network_price, call_out_network_price, sms_in_network_price, " +
            "sms_out_network_price, chars_in_sms from network_provider where id = (select provider_id from phone where user_id=?)";

    public NetworkProvider getNetworkProvider(User user) throws SQLException {
        NetworkProvider networkProvider = null;
        Connection connection = DBconnection.getDBConnection();
        ResultSet rs = null;
        try {
            PreparedStatement ps = connection.prepareStatement(GET_PROVIDER);
            ps.setInt(1, user.getId());
            rs = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (rs.next()) {
            networkProvider = (NetworkProvider) new DataConverter(NetworkProvider.class).fromResultSetToEntity(rs);
        }
        return networkProvider;
    }

    public int detectNetworkProvider(String number) {
        String kyivstarPhoneNumberPattern = "(^[0])+([6|9])+([6|7|8])+ *\\d{7}";
        String lifePhoneNumberPattern = "(^[0])+([6|7|9])+([3])+ *\\d{7}";
        Pattern p1 = Pattern.compile(kyivstarPhoneNumberPattern);
        Pattern p2 = Pattern.compile(lifePhoneNumberPattern);
        Matcher matcher1 = p1.matcher(number);
        Matcher matcher2 = p2.matcher(number);
        if (matcher1.find()) {
            logger.trace("Your network provider is Kyivstar\n");
            return 1;
        } else if (matcher2.find()) {
            logger.trace("Your network provider is Life\n");
            return 2;
        } else {
            logger.trace("Your network provider is Unknown\n");
            return 3;
        }
    }
}
