package service;

import dbconnection.DBconnection;
import dbconnection.dbdata.DataConverter;
import model.Call;
import model.SMS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PhoneService {
    public static final Logger logger = LogManager.getLogger(PhoneService.class);
    public static final String ADD_CALL = "insert into calls (receiver, call_time, user_id) values(?,?,?)";
    public static final String GET_CALLS = "select id, receiver, call_time, user_id from calls where user_id=? order by id desc";
    public static final String ADD_SMS = "insert into sms (receiver, sms_text, user_id) values(?,?,?)";
    public static final String GET_SMS = "select id, receiver, sms_text, user_id from sms where user_id=? order by id desc";

    public int registerCall(String receiver, int callTime, int userID){
        Connection connection = DBconnection.getDBConnection();
        int countOfRowsUpdated = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(ADD_CALL);
            ps.setString(1, receiver);
            ps.setInt(2, callTime);
            ps.setInt(3, userID);
            countOfRowsUpdated = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return countOfRowsUpdated;
    }

    public List<Call> getCallHistory(int userID) throws SQLException {
        List<Call> calls = new ArrayList<>();
        Connection connection = DBconnection.getDBConnection();
        ResultSet rs = null;
        try {
            PreparedStatement ps = connection.prepareStatement(GET_CALLS);
            ps.setInt(1, userID);
            rs = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (rs.next()){
            calls.add((Call) new DataConverter(Call.class).fromResultSetToEntity(rs));
        }
        return calls;
    }

    public int registerSMS(String receiverNumber, String smsText, int userID) {
        Connection connection = DBconnection.getDBConnection();
        int countOfRowsUpdated = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(ADD_SMS);
            ps.setString(1, receiverNumber);
            ps.setString(2, smsText);
            ps.setInt(3, userID);
            countOfRowsUpdated = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return countOfRowsUpdated;
    }

    public List<SMS> getSmsHistory(int userID) throws SQLException {
        List<SMS> sms = new ArrayList<>();
        Connection connection = DBconnection.getDBConnection();
        ResultSet rs = null;
        try {
            PreparedStatement ps = connection.prepareStatement(GET_SMS);
            ps.setInt(1, userID);
            rs = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (rs.next()) {
            sms.add((SMS) new DataConverter(SMS.class).fromResultSetToEntity(rs));
        }
        return sms;
    }
}
