package service;

import dbconnection.DBconnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RegisterService {
    public static final String CREATE_USER = "insert into user (name, surname, username, password) values (?, ?, ?, ?)";
    public static final String CREATE_PHONE = "insert into phone (model, number, user_id, provider_id) values (?, ?, ?, ?)";
    public static final String GET_USER_ID = "select id from user where name=? and surname=? and username=? and password=?";

    public int createUser(String name, String surname, String username, String password){
        Connection connection = DBconnection.getDBConnection();
        int countOfRowsUpdated = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(CREATE_USER);
            ps.setString(1, name);
            ps.setString(2, surname);
            ps.setString(3, username);
            ps.setString(4, password);
            countOfRowsUpdated = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return countOfRowsUpdated;
    }

    public int createPhone(String name, String surname, String username, String password, String model,String number, int networkProviderId){
        Connection connection = DBconnection.getDBConnection();
        int countOfRowsUpdated = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(CREATE_PHONE);
            ps.setString(1, model);
            ps.setString(2, number);
            ps.setInt(3, getUserID(name, surname, username, password));
            ps.setInt(4, networkProviderId);
            countOfRowsUpdated = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return countOfRowsUpdated;
    }

    public int getUserID(String name, String surname, String username, String password){
        Connection connection = DBconnection.getDBConnection();
        int user_id = 0;
        ResultSet rs = null;
        try {
            PreparedStatement ps = connection.prepareStatement(GET_USER_ID);
            ps.setString(1, name);
            ps.setString(2, surname);
            ps.setString(3, username);
            ps.setString(4, password);
            rs = ps.executeQuery();
            while (rs.next()){
                user_id = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user_id;
    }
}
