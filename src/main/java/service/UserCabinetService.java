package service;

import dbconnection.DBconnection;
import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserCabinetService {
    public static final String GET_BALANCE = "select balance from phone where user_id=?";
    public static final String UPDATE_BALANCE = "update phone set balance=? where id=?";
    public static final String GET_PHONE_ID = "select id from phone where user_id=?";

    public double getBalance(User user){
        Connection connection = DBconnection.getDBConnection();
        ResultSet rs = null;
        double balance = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(GET_BALANCE);
            ps.setInt(1, user.getId());
            rs = ps.executeQuery();
            while (rs.next()){
                balance = rs.getDouble("balance");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return balance;
    }

    public int refillBalance(User user, double amountToAdd){
        int rowsUpdated = 0;
        ResultSet rs = null;
        Connection connection = DBconnection.getDBConnection();
        try {
            PreparedStatement ps = connection.prepareStatement(UPDATE_BALANCE);
            ps.setDouble(1, getBalance(user) + amountToAdd);
            ps.setInt(2, getPhoneID(user));
            rowsUpdated = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowsUpdated;
    }

    public int updateBalance(User user, double newBalance){
        int rowsUpdated = 0;
        ResultSet rs = null;
        Connection connection = DBconnection.getDBConnection();
        try {
            PreparedStatement ps = connection.prepareStatement(UPDATE_BALANCE);
            ps.setDouble(1, newBalance);
            ps.setInt(2, getPhoneID(user));
            rowsUpdated = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowsUpdated;
    }

    public int deductBalance(User user, double amoutToDeduct){
        double finalBalance = 0;
        if ((getBalance(user) - amoutToDeduct) < 0){
            finalBalance = 0.0;
        }else {
            finalBalance = getBalance(user) - amoutToDeduct;
        }
        int rowsUpdated = 0;
        ResultSet rs = null;
        Connection connection = DBconnection.getDBConnection();
        try {
            PreparedStatement ps = connection.prepareStatement(UPDATE_BALANCE);
            ps.setDouble(1, finalBalance);
            ps.setInt(2, getPhoneID(user));
            rowsUpdated = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowsUpdated;
    }

    public int getPhoneID(User user) throws SQLException {
        int phoneID = 0;
        ResultSet rs = null;
        Connection connection = DBconnection.getDBConnection();
        try {
            PreparedStatement ps = connection.prepareStatement(GET_PHONE_ID);
            ps.setInt(1, user.getId());
            rs = ps.executeQuery();
            while (rs.next()){
                phoneID = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return phoneID;
    }
}
