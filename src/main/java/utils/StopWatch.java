package utils;

public class StopWatch {
    private final long start;

    public StopWatch() {
        this.start = System.currentTimeMillis();
    }

    public double getTime() {
        long now = System.currentTimeMillis();
        return (((now - start) / 1000.0));
    }
}
