package view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import view.menu.WelcomeMenu;

import java.util.Scanner;

public class View {
    public static final Logger logger = LogManager.getLogger(View.class);
    Scanner scanner = new Scanner(System.in);
    int key;

    public void start() {
        do {
            new WelcomeMenu().print();
            logger.debug("Please make your choice");
            try {
                key = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                logger.error(e.toString());
                continue;
            }
            if (!new WelcomeMenu().welcomeMethodMenu.containsKey(key)) {
                logger.error("You entered incorrect data\n\t\tPlease try again");
                continue;
            }
            new WelcomeMenu().welcomeMethodMenu.get(key).print();
        } while (key != 3);
    }
}
