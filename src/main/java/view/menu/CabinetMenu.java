package view.menu;

import controller.CabinetMenuController;
import controller.Controller;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CabinetMenu {
    public static final Logger logger = LogManager.getLogger(CabinetMenu.class);
    public Map<Integer, String> cabinetMenu;
    public Map<Integer, Controller> cabinetMethodMenu;
    private User user;

    public CabinetMenu(User user) {
        this.user = user;
        cabinetMenu = new HashMap<>();
        cabinetMethodMenu = new HashMap<>();

        cabinetMenu.put(1, "Get account info");
        cabinetMenu.put(2, "Show balance");
        cabinetMenu.put(3, "Refill balance");
        cabinetMenu.put(4, "Make a call");
        cabinetMenu.put(5, "Get call history");
        cabinetMenu.put(6, "Send SMS");
        cabinetMenu.put(7, "Get SMS history");
        cabinetMenu.put(8, "Log out");

        cabinetMethodMenu.put(1, new CabinetMenuController(user)::getAccountInfo);
        cabinetMethodMenu.put(2, new CabinetMenuController(user)::showBalance);
        cabinetMethodMenu.put(3, new CabinetMenuController(user)::refillBalance);
        cabinetMethodMenu.put(4, new CabinetMenuController(user)::makeCall);
        cabinetMethodMenu.put(5, new CabinetMenuController(user)::getCallHistory);
        cabinetMethodMenu.put(6, new CabinetMenuController(user)::sendSMS);
        cabinetMethodMenu.put(7, new CabinetMenuController(user)::getSmsHistory);
        cabinetMethodMenu.put(8, new CabinetMenuController()::logOut);

    }

    public void outputSubCabinetMenu() {
        Scanner scanner = new Scanner(System.in);
        int key = -1;
        do {
            print();
            logger.debug("Please make your choice");
            try {
                key = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                logger.error(e.toString());
                continue;
            }
            if (!this.cabinetMethodMenu.containsKey(key)) {
                logger.error("You entered incorrect data" + "\nPlease try again");
                continue;
            }
            this.cabinetMethodMenu.get(key).print();
        } while (key != 8);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void print() {
        cabinetMenu.forEach((k, v) -> logger.info(k + " - " + v));
    }
}
