package view.menu;

import controller.Controller;
import controller.LoginMenuController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class LoginMenu {
    public static final Logger logger = LogManager.getLogger(RegisterMenu.class);
    public Map<Integer, String> loginMenu;
    public Map<Integer, Controller> loginMethodMenu;

    public LoginMenu() {
        loginMenu = new HashMap<>();
        loginMethodMenu = new HashMap<>();

        loginMenu.put(1, "Enter user credentials");
        loginMenu.put(2, "Back");

        loginMethodMenu.put(1, new LoginMenuController()::provideUserCredentials);
        loginMethodMenu.put(2, new LoginMenuController()::back);


    }

    public void outputSubLoginMenu() {
        Scanner scanner = new Scanner(System.in);
        int key = -1;
        do {
            print();
            logger.debug("Please make your choice");
            try {
                key = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                logger.error(e.toString());
                continue;
            }
            if (!this.loginMethodMenu.containsKey(key)) {
                logger.error("You entered incorrect data" + "\nPlease try again");
                continue;
            }
            this.loginMethodMenu.get(key).print();
        } while (key != 2);

    }

    public void print() {
        loginMenu.forEach((k, v) -> logger.info(k + " - " + v));
    }
}
