package view.menu;

import controller.Controller;
import controller.RegisterMenuController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class RegisterMenu {
    public static final Logger logger = LogManager.getLogger(RegisterMenu.class);
    public Map<Integer, String> registerMenu;
    public Map<Integer, Controller> registerMethodMenu;

    public RegisterMenu(){
        registerMenu = new HashMap<>();
        registerMethodMenu = new HashMap<>();

        registerMenu.put(1, "Provide user details");
        registerMenu.put(2, "Back");

        registerMethodMenu.put(1, new RegisterMenuController()::createUser);
        registerMethodMenu.put(2, new RegisterMenuController()::back);

    }

    public void outputSubRegisterMenu(){
        Scanner scanner = new Scanner(System.in);
        int key = -1;
        do {
            print();
            logger.trace("Please make your choice");
            try {
                key = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                logger.error(e.toString());
                continue;
            }
            if (!this.registerMethodMenu.containsKey(key)) {
                logger.error("You entered incorrect data" + "\nPlease try again");
                continue;
            }
            this.registerMethodMenu.get(key).print();
        } while (key != 2);
    }

    public void print() {
        registerMenu.forEach((k,v)->logger.info(k + " - " + v));
    }
}
