package view.menu;

import controller.Controller;
import controller.WelcomeMenuController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class WelcomeMenu {
    public static final Logger logger = LogManager.getLogger(WelcomeMenu.class);
    public Map<Integer, String> welcomeMenu;
    public Map<Integer, Controller> welcomeMethodMenu;

    public WelcomeMenu(){
        welcomeMenu = new HashMap<>();
        welcomeMethodMenu = new HashMap<>();

        welcomeMenu.put(1, "Login");
        welcomeMenu.put(2, "Register а new customer");
        welcomeMenu.put(3, "Quit");

        welcomeMethodMenu.put(1, new LoginMenu()::outputSubLoginMenu);
        welcomeMethodMenu.put(2, new RegisterMenu()::outputSubRegisterMenu);
        welcomeMethodMenu.put(3, new WelcomeMenuController()::quit);
    }

    public void print() {
        logger.debug("Welcome to Cabinet!");
        welcomeMenu.forEach((k,v)->logger.info(k + " - " + v));
    }
}
